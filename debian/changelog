net-cpp (3.1.1+dfsg-4) unstable; urgency=medium

  * debian/patches:
    + Simplify 1007_wait-for-flask.patch and use already present sleep_for()
      in tests/httpbin.h.in, increase it from 1sec to 5secs.
    + Trivially rebase 2001_no-tests-that-require-inet-access.patch.
    + Add 1008_set-MAX-CONTENT-LENGTH-for-flask-server-to-let-it-accept-big-
      chunks-of-data.patch. Set MAX_FORM_MEMORY_SIZE to a limit that can
      also handle the HttpClient.post_request_for_file_with_large_chunk_succeeds
      test. (Closes: #1088721).
  * debian/control:
    + In B-D: field, switch from pkg-config to pkgconf.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 12 Dec 2024 22:42:20 +0100

net-cpp (3.1.1+dfsg-3) unstable; urgency=medium

  * debian/libnet-cpp2.symbols:
    + Drop file. (Closes: #1015555).
  * debian/control:
    + Bump Standards-Version to 4.7.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 16 Aug 2024 09:56:57 +0200

net-cpp (3.1.1+dfsg-2) unstable; urgency=medium

  * debian/watch:
    + Update file for recent GitLab changes of the tags overview page.
  * debian/copyright:
    + Drop Files-Excluded: line in top header. httpbin.tar.bz2 has been dropped
      upstream.
  * debian/control:
    + Versionize B-D on libprocess-cpp-dev. Require process-cpp (>= 3.0.2)
      (which comes with a fixed SOVERSION_MAJOR).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 16 Aug 2024 09:50:17 +0200

net-cpp (3.1.1+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 0003-tests-JSON_CPP-and-PROCESS_CPP-are-required-deps.patch
      and 0004_CMakeLists.txt-Add-CMake-flag-ENABLE_WERROR-defaulti.patch.
      Applied upstream.
  * debian/copyright:
    + Update copyright attribution for debian/.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 01 Feb 2024 17:57:26 +0100

net-cpp (3.1.0+dfsg-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster (oldstable)
  * Apply multi-arch hints. + libnet-cpp-doc: Add Multi-Arch: foreign.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository.
  * Remove obsolete field Name from debian/upstream/metadata (already present in machine-readable debian/copyright).
  * Remove unnecessary get-orig-source-target.
  * Update standards version to 4.6.2, no changes needed.

  [ Mike Gabriel ]
  * Revert "Remove unnecessary get-orig-source-target."

  [ Marius Gripsgard ]
  * debian/patches: Add patch to bump c++ std version to 14 (closes: #1041122)

 -- Marius Gripsgard <mariogrip@debian.org>  Wed, 26 Jul 2023 04:12:04 +0200

net-cpp (3.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop patches 0001, 0002, 1004 and 1006. Applied upstream.
    + Add 0004_CMakeLists.txt-Add-CMake-flag-ENABLE_WERROR-defaulti.patch.
      Disable -Werror for now via CMake option. (Closes: #1027648).
  * debian/copyright:
    + Update attributions for debian/.
    + Add autogenerated copyright.in file for later copyright reference
      tracking.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 02 Jan 2023 10:52:09 +0100

net-cpp (3.0.0+dfsg1-3) unstable; urgency=medium

  * debian/:
    + Update upstream Git repo URL.
  * debian/control:
    + Bump Standards-Version: to 4.6.1. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Jul 2022 17:42:57 +0200

net-cpp (3.0.0+dfsg1-2) unstable; urgency=medium

  * debian/patches:
    + Grab patches from upstream to fix newer liburl and fix tests
      (Closes: #1005475)
    + Refresh upstreamed patches
  * debian/control: Add myself as uploader

 -- Marius Gripsgard <marius@ubports.com>  Sat, 12 Mar 2022 00:52:09 +0100

net-cpp (3.0.0+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop patches 1001, 1002, 1003 and 1005. All applied upstream.
  * debian/control:
    + Bump Standards-Version: to 4.6.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 02 Dec 2021 08:06:02 +0100

net-cpp (2.2.1+dfsg1-6) unstable; urgency=medium

  * debian/rules:
    + Add dh_clean override, remove results.txt. This makes the build
      idempotent.
  * debian/control:
    + Switch from transitional google-mock to B-D googletest.
      Additionally add B-D libgtest-dev for GMock module in
      cmake-extras to be happy.
    + Update versioned B-D: cmake-extras (>= 1.5-5~). (Closes: #973214).
    + Bump to DH compat level version 13.
  * debian/libnet-cpp2.symbols:
    + Fix package name (add missing SOVERSION to the name).
  * debian/patches:
    + Forward patches that are non-specific for Debian to upstream.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 30 Oct 2020 08:46:52 +0100

net-cpp (2.2.1+dfsg1-5) unstable; urgency=medium

  * debian/changelog:
    + Typo fix in previous upload's stanza.
  * debian/patches/README:
    + Add file, explaining our patch naming scheme.
  * debian/patches:
    + Add 2001_no-tests-that-require-inet-access.patch. Disable tests that
      require internet access. (Closes: #964212).

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 13 Jul 2020 12:41:20 +0200

net-cpp (2.2.1+dfsg1-4) unstable; urgency=medium

  * debian/libnet-cpp2.symbols:
    + Fix FTBFS on i386 m68k powerpc x32.
  * debian/patches:
    + Add 1007_wait-for-flask.patch. On slow build systems (e.g. armel, armhf,
      etc.), the tests run too early and the flask http server does not have
      enough time to come up. This patch injects an artificial delay of 3
      seconds between http server startup and client tests.

 -- Mike Gabriel <mike.gabriel@das-netzwerkteam.de>  Tue, 18 Feb 2020 13:24:20 +0100

net-cpp (2.2.1+dfsg1-3) unstable; urgency=medium

  * debian/control:
    + Add B-D pkg-kde-tools.
  * debian/rules:
    + Build --with pkgkde_symbolshelper.
  * debian/libnet-cpp2.symbols:
    + Update using pkg-kde-tools.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 17 Feb 2020 22:14:59 +0000

net-cpp (2.2.1+dfsg1-2) unstable; urgency=medium

  * debian/rules:
   + Only fix duplicate files in doc/html/ if that folder actually
     exists. Fixes binary-only DEB builds.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 03 Feb 2020 10:56:36 +0100

net-cpp (2.2.1+dfsg1-1) unstable; urgency=medium

  * Initial upload to Debian. (Closes: #950434).
  * debian/patches:
    + Add 1001_no-bundled-httpbin.patch,
          1002_set-content-type-header-on-POSTs.patch,
          1003_port-to-new-json-cpp-API.patch.
      Fix multi-cause FTBFSes. Thanks to Luca Weiss for providing them.
    + Add 1004_INSTANTIATE_TEST_SUITE.patch. Fix Boost deprecation warnings.
    + Add 1005_typo-fix.patch. Typo fix in printed output.
    + Add 1006_doxygen-no-buildpath.patch. No FULL_PATH_NAMES in API docs.
  * debian/watch:
    + Add watch file.
  * debian/{control,compat}:
    + Switch to debhelper-compat notation. Bump to DH compat level version 12.
  * debian/control:
    + Update Maintainer: and Uploaders: fields.
    + Debian buildds don't support alternative B-Ds.
    + Build-depend on unversioned Boost library packages.
    + Bump Standards-Version: to 4.5.0. No changes needed.
    + Add B-D: cmake-extras.
    + Use system-wide python3-httpbin during unit tests. Add that to B-D.
    + Update Homepage: field (new upstream location).
    + Update Vcs-*: fields for Debian Git packaging on salsa.debian.org.
    + Fix SYNOPSIS of libnet-cpp-dev bin:pkg.
    + Lengthen the LONG_DESCRIPTION of libnet-cpp-doc a little bit to silence
      lintian.
    + Add Rules-Requires-Root: field and set it to 'no'.
    + Add B-Ds: rdfind, symlinks.
  * debian/upstream/metadata:
    + Add file; comply with DEP-12.
  * debian/source/format:
    + Add format file; 3.0 (quilt).
  * debian/copyright:
    + Update Source: field with new upstream location.
    + Update copyright attributions for debian/*.
    + Use secure URL in Format: field.
  * debian/rules:
    + Run unit tests in non-parallel mode.
    + Introduce dh_missing override (--fail-missing).
    + Add get-orig-source target.
    + Enable all hardening build flags.
    + Fix doc:pkg file location, get rid of duplicate files in the API docs.
  * debian/libnet-cpp2.symbols:
    + Add Build-Depends-Package: metadata field.
    + Drop -0ubuntu1 from code versions.
  * debian/libnet-cpp1.symbols:
    + Drop file. Left-over from before the last SONAME bump.
  * debian/libnet-cpp-doc.doc-base:
    + Add file.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 01 Feb 2020 23:38:36 +0100

net-cpp (2.2.1+ubports) bionic; urgency=medium

  * Imported to UBports

 -- UBports auto importer <infra@ubports.com>  Mon, 09 Apr 2018 14:48:49 +0200

net-cpp (2.2.0+17.04.20161108.2-0ubuntu1) zesty; urgency=medium

  [ Gary Wang ]
  * Fix http header parsing in net response.
    (LP: #1631846)

 -- Gary.Wang <gary.wang@canonical.com>  Tue, 08 Nov 2016 09:21:39 +0000

net-cpp (2.1.0+16.10.20160913.2-0ubuntu1) yakkety; urgency=medium

  [ Thomas Voß ]
  [Gary Wang]
  * Enable pause/resume of requests.
  * Fix LP:#1570686 and LP:#1570687

  [ Gary Wang ]
  * Add data reading callback function in streaming http interface.
  * Fix LP:#1605179

 -- Gary.Wang <gary.wang@canonical.com>  Tue, 13 Sep 2016 08:55:30 +0000

net-cpp (2.0.0-0ubuntu2) yakkety; urgency=medium

  * No-change rebuild for boost soname change.

 -- Matthias Klose <doko@ubuntu.com>  Sat, 23 Apr 2016 18:46:28 +0000

net-cpp (2.0.0-0ubuntu1) wily; urgency=medium

  * Bump version.

 -- Thomas Voß <thomas.voss@canonical.com>  Tue, 21 Jul 2015 11:44:30 +0200

net-cpp (2.0.0) wily; urgency=medium

  * Bump major revision to account for toolchain update. Fixes LP:#1452333.

 -- Thomas Voß <thomas.voss@canonical.com>  Mon, 20 Jul 2015 21:11:18 +0200

net-cpp (1.2.0+15.04.20150415.2-0ubuntu1) vivid; urgency=medium

  [ Thomas Voß ]
  * Introduce a streaming http interface.

 -- CI Train Bot <ci-train-bot@canonical.com>  Wed, 15 Apr 2015 12:34:49 +0000

net-cpp (1.1.0+15.04.20150305-0ubuntu1) vivid; urgency=medium

  [ thomas-voss ]
  * Make sure that Multi::Private instances are correctly cleaned up by
    only handing out weak_ptr's to it. (LP: #1419620, #1423765)

 -- CI Train Bot <ci-train-bot@canonical.com>  Thu, 05 Mar 2015 12:08:09 +0000

net-cpp (1.1.0+15.04.20150123-0ubuntu1) vivid; urgency=low

  [ Ubuntu daily release ]
  * New rebuild forced

  [ Marcus Tomlinson ]
  * Explicitly cast milliseconds::count() to long.

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 23 Jan 2015 10:17:07 +0000

net-cpp (1.1.0+15.04.20141204-0ubuntu1) vivid; urgency=low

  [ Ubuntu daily release ]
  * debian/libnet-cpp1.symbols: auto-update to released version
  * New rebuild forced

  [ thomas-voss ]
  * Robustify header line parser against empty values in header lines.
    Add test case to verify that empty header value handling is correct.
    (LP: #1392315)

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Thu, 04 Dec 2014 20:54:29 +0000

net-cpp (1.1.0+14.10.20140804-0ubuntu1) utopic; urgency=medium

  [ Pete Woods ]
  * Add Uri class and corresponding string conversion.

  [ Ubuntu daily release ]
  * debian/libnet-cpp1.symbols: auto-update to released version

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Mon, 04 Aug 2014 09:41:07 +0000

net-cpp (1.0.0+14.10.20140729.1-0ubuntu1) utopic; urgency=low

  [ Pete Woods ]
  * No change rebuild

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Tue, 29 Jul 2014 10:18:39 +0000

net-cpp (1.0.0+14.10.20140718-0ubuntu1) utopic; urgency=medium

  [ Thomas Voß ]
  * Bump major revision and so name to account for toolchain update.

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Fri, 18 Jul 2014 12:38:11 +0000

net-cpp (0.0.1+14.10.20140611-0ubuntu1) utopic; urgency=low

  [ Thomas Voß ]
  * Initial release.

  [ Ubuntu daily release ]
  * debian/*symbols: auto-update new symbols to released version

  [ thomas-voss ]
  * Initial checkin.
  * Adjusted URI parsing.
  * Add documentation for the Uri class.
  * Added an exception for signalling parsing errors.
  * GET, POST, PUT, HEAD now works.
  * Add tests working against the mozilla location service. Add a
    missing handle reset for handles recycled from the pool.
  * Add curl multi support.
  * Cleanup curl based implementatoin and split out easy, multi and
    shared into their own header/implementation files. Provide an async
    interface to executing requests. Provide basic and digest auth
    support.
  * Cleanup error handling. Add documentation for
    core::net::http::Request.
  * Add documentation and clean up.
  * Add load tests and statistics.
  * Add debian packaging. Add timeout for requests. Add cancel
    functionality for requests.
  * Add pkg-config setup.
  * Add doc package. Add symbols file and symbols map.
  * Enable ssl default engine. Disable host and peer ssl checks.
  * Make sure that a request survives an async execution.
  * Add libcurl4-openssl-dev as a build-dependency. Ensure that no
    signals are being caught by curl. Switch to production url for
    Mozilla's location service.
  * Get rid of custom Uri class, not needed right now. Add custom header
    support for http requests.
  * Make test-cases standalone by running a local instance of httpbin.
  * Add bzr builddeb configuration.
  * Remove references to core::net::Uri.
  * Remove further references to core::net::Uri.
  * Adjust path to server executable in tests. Add missing build
    dependencies.
  * Make sure that we do not block indefinitely waiting for the httpbin
    instance to be torn down.
  * Make sure to strip leading and trailing whitespace from header
    fields.
  * Make sure that we kill the entire python process group running the
    httpbin instance.
  * Run httpbin without the flask-weirdness to allow for killing it with
    a vanilla unix signal. Make sure we execute all the right tests :-)
  * Disable test case running against the Ubuntu app store.
  * Merged lp:~pete-woods/net-cpp/base64

  [ CI bot ]
  * Empty MP for landing.

 -- Ubuntu daily release <ps-jenkins@lists.canonical.com>  Wed, 11 Jun 2014 10:28:20 +0000
